import sys
import os.path
import socket
import time
import _thread


class HttpServer(object):
    MIME_TYPES = "mime.types"
    HTTP_CONF = "http.conf"

    RESPONSE_MESSAGE = (
        'HTTP/1.1 200 OK\r\n'
        'Connection: close\r\n'
        'Content-Length: {0}\r\n'
        'Content-Type: {1}\r\n'
        'Date: {2}\r\n'
        '\r\n'
    )

    RESPONSE_ERROR_MESSAGE = (
        'HTTP/1.1 {0} {1}\r\n'
        'Connection: close\r\n'
        'Date: {2}\r\n\r\n'
        '<!DOCTYPE html>'
        '<html><body><head><center><h1>'
        '404 Not Found'
        '</h1></center></head></body></html>'
    )

    def __init__(self):
        self.conf_map = self._get_http_conf()
        self.mime_map = self._get_mime_type()
        self.root_dir = self.conf_map["root_dir"]
        self.port = int(self.conf_map["port"])

        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._sock.bind(('', self.port))
        self._sock.listen(5)

    def serve_forever(self):
        while True:
            conn, addr = self._sock.accept()
            # self.conn = conn
            _thread.start_new_thread(self._handle_request, (conn,))

    def _handle_request(self, conn):
        data = conn.recv(1024).decode("utf-8")

        attrs = self._parse(data)
        if len(attrs) != 3:
            return
        method = attrs[0]
        tail_path = attrs[1]

        if method != 'GET':
            msg = self._gen_error(405, 'Method Not Allowed')
            conn.send(msg)
            conn.close()
            return

        self._do_GET(conn, tail_path)

    def _parse(self, data):
        start_line = data.split('\r\n')[0]
        return start_line.split(" ")

    def _do_GET(self, conn, tail_path):
        full_path = self._get_full_path(tail_path)
        log_templ = "{} {}"

        if not os.path.isfile(full_path):
            msg = self._gen_error(404, "Not Found")
            log = log_templ.format(404, "/some-strange-url.notfound")
        else:
            msg = self._gen_response(full_path)
            log = log_templ.format(200, tail_path)
        conn.send(msg)
        conn.close()
        print(log)

    def _get_full_path(self, tail_path):
        if tail_path == '/':
            tail_path = '/index.html'

        return self.root_dir + tail_path[1:]

    def _date_time_string(self):
        from time import gmtime, strftime

        date_time = strftime("%a, %d %b %Y %X GMT", gmtime())
        return date_time

    def _gen_response(self, full_path):
        with open(full_path, 'rb') as f:
            body = f.read()
        extension = full_path[full_path.find('.') + 1:]
        mimetype = self._guess_mime_type(extension)
        date = self._date_time_string()

        msg_hat = self.RESPONSE_MESSAGE.format(
            len(body),
            mimetype,
            date,
        )

        return msg_hat.encode('utf-8') + body

    def _gen_error(self, code, message):
        date = self._date_time_string()
        msg = self.RESPONSE_ERROR_MESSAGE.format(code, message, date)
        return msg.encode('utf-8')

    def _guess_mime_type(self, extension):
        if extension in self.mime_map:
            return self.mime_map[extension]
        return "application/octet-stream"

    def _get_mime_type(self):
        return self._read_file2map(self.MIME_TYPES)

    def _get_http_conf(self):
        return self._read_file2map(self.HTTP_CONF)

    def _read_file2map(self, file_name):
        file_map = {}
        with open(file_name, 'r') as f:
            for line in f:
                if line != '\n':
                    i = line.find(' ')
                    file_map[line[:i]] = line[i:].lstrip(' ').rstrip('\n')
        return file_map

if __name__ == '__main__':
    server = HttpServer()
    server.serve_forever()
    server.close()
